def measure (count=1)
  start = Time.now
  count.times {yield}
  finish= Time.now
  (finish - start) / count
end
