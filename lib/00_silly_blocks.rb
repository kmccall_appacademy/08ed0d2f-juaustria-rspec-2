def reverser
  yield.split(" ").map(&:reverse).join(" ")
end

def adder(to_add=1)
  yield + to_add
end

def repeater(repeat_count=1)
   repeat_count.times { yield }
end
